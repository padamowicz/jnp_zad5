#ifndef VIRUS_GENEALOGY_H_
#define VIRUS_GENEALOGY_H_

#include <vector>
#include <map>
#include <set>
#include <stdexcept>
#include <memory>
 
class TriedToRemoveStemVirus : public std::exception {
public:
    TriedToRemoveStemVirus() : std::exception() {};
    virtual const char* what() const noexcept {
        return "TriedToRemoveStemVirus";
    }
};
 
class VirusAlreadyCreated : public std::exception {
public:
    VirusAlreadyCreated() : std::exception() {};
    virtual const char* what() const noexcept {
        return "VirusAlreadyCreated";
    }
};
 
class VirusNotFound : public std::exception {
public:
    VirusNotFound() : std::exception() {};
    virtual const char* what() const noexcept {
        return "VirusNotFound";
    }
};
 
template <class Virus>
class VirusGenealogy {

public:
    typedef typename Virus::id_type vid_type;

private:
    class Node;
    typedef std::shared_ptr<Node> NodePtr;
    typedef std::weak_ptr<Node> WeakNodePtr;
    typedef std::map<vid_type, WeakNodePtr> map_of_viruses;
    
    vid_type stem_id;
    NodePtr stem_virus;
    map_of_viruses viruses;

    class Node {
    public:
        vid_type id;
        Virus virus;
        map_of_viruses *my_genealogy;

        std::set<WeakNodePtr, std::owner_less<WeakNodePtr>> parents;
        std::set<NodePtr> children;
        Node(vid_type const &stem_id, map_of_viruses *g): id(stem_id), virus(Virus(stem_id)), my_genealogy(g) {};
        ~Node() {
            my_genealogy->erase(id);
        };
    };

public:
    VirusGenealogy & operator=(const VirusGenealogy &) = delete;
    VirusGenealogy(const VirusGenealogy &) = delete;

    ~VirusGenealogy() {
        stem_virus.reset();
    }

   VirusGenealogy(vid_type const &stem_id): stem_id(stem_id), stem_virus(new Node(stem_id, &viruses)) {
        viruses.emplace(stem_id, WeakNodePtr(stem_virus));
    }

    vid_type get_stem_id() const noexcept {
        return stem_id; 
    };
 
    std::vector<vid_type> get_children(vid_type const &id) const {
        if(!exists(id))
            throw VirusNotFound();
 
        std::vector<vid_type> temp;
        for(auto node : viruses.at(id).lock()->children)
            temp.push_back(node->id);
        return temp;
    };
 
    std::vector<vid_type> get_parents(vid_type const &id) const {
        if(!exists(id))
            throw VirusNotFound();
        
        std::vector<vid_type> temp;
        for(auto node : viruses.at(id).lock()->parents)
            temp.push_back(node.lock()->id);
        return temp;
    };
 
    bool exists(vid_type const &id) const {
        return viruses.find(id) != viruses.end();
    };
 
    Virus& operator[](vid_type const &id) const {
        if (!exists(id))
            throw VirusNotFound();
        
        return viruses.at(id).lock()->virus;
    };
 
    void create(vid_type const &id, vid_type const &parent_id) {
        create(id, std::vector<vid_type>({parent_id}));
    };
 
    void create(vid_type const &id, std::vector<vid_type> const &parent_ids) {
        if (exists(id))
            throw VirusAlreadyCreated();
        
        if(parent_ids.empty())
            throw VirusNotFound();
        
        for(auto &p_id: parent_ids) {
            if(!exists(p_id))
                throw VirusNotFound();
        }

        NodePtr temp(new Node(id, &viruses));

        try {
            viruses.emplace(id, WeakNodePtr(temp));
            for(auto &parent: parent_ids)
                connect(id, parent);
        } catch (...) {
            for(auto &parent: parent_ids)
                viruses.at(parent).lock()->children.erase(NodePtr(temp));
            viruses.erase(id);
            throw;
        }
    };
 
    void connect(vid_type const &child_id, vid_type const &parent_id) {
        if(!exists(child_id) || !exists(parent_id))
            throw VirusNotFound();

        NodePtr parent = viruses[parent_id].lock();
        NodePtr child = viruses[child_id].lock();
        
        parent->children.insert(child);
        
        try {
            child->parents.insert(parent);
        } catch (...) {
            parent->children.erase(child);
            throw;
        }
    };
 
    void remove(vid_type const &id) {
        if (!exists(id))
            throw VirusNotFound();
        if (id == stem_id)
            throw TriedToRemoveStemVirus();
        
        NodePtr node = viruses[id].lock();
        for(auto &parent : node->parents)
            parent.lock()->children.erase(node);
    };
};

#endif // VIRUS_GENEALOGY_H_
